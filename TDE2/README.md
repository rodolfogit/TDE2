## TDE 2 - 201801 - OOP
Você recebeu um projeto inacabado de um sistema banc&aacute;rio.

Sua obriga&ccedil;&atilde;o &eacute;:

   - corrigir os erros encontrados
   - atender as solicitações marcadas com a tag TODO, sem exceção   
   - Implementar o que for necessário
   - A classe Principal também deve ser corrigida - caso necessário - e rodar de acordo
  
Essa tarefa contempla

   - Encapsulamento
   - Generics
   - Loops/La&ccedil;os **for, while **
   - Incrementos e sobrecargas, como ** ++ , -= , += **
   - Padr&otilde;es b&aacute;sicos de projeto Java
   - Uso do GIT - tamb&eacute;m j&aacute; ensinado em aula

  Dica:
  
   - **Use a força Luke**, leia os slides no classroom
   - **Leia as instru&ccedil;&otilde;es abaixo de como importar o projeto para o Eclipse**
   
Como será trabalhado o projeto?

   1. Faça um [fork](https://gitlab.com/cest-oop-201801/tde/TDE2/forks/new) do [projeto](https://gitlab.com/cest-oop-201801/tde/TDE2) - ![Fork](/static/rect881.png)
   	 - Isso far&aacute; uma c&oacute;pia do projeto s&oacute; para voc&ecirc; e permitir&aacute;, caso necess&aacute;rio que eu coloque corre&ccedil;&otilde;es no projeto e todos recebam.
   	 - Outra vantagem &eacute; que suas altera&ccedil;&otilde;es n&atilde;o interferem no projeto dos outros alunos
   1. Importar o projeto para o Eclipse necess&aacute;rio &eacute; em (File > Import) ![Wizard_Import](/static/wz_import.png)
   1. Escolha na &aacute;rvore a pasta (Git > Projects from Git) ![git_import](/static/git_project.png)
   1. Na tela seguinte **selecione** (Clone URI) ![git_wz_clone](/static/git_wizard_clone_uri.png) 
   1. Aparecer&aacute; um Wizard onde voc&ecirc; dever&aacute; colocar a URI do seu projeto no GitLab.
    1. Na figura o campo URI está com um endere&ccedi;o de exemplo, **substitua colocando a URI do projeto que voc&ecirc; acabou de criar no procedimento do FORK**. ![git_wizard](/static/git_wizard_define_uri.png)
   1. O clone ir&aacute; importar o projeto para um diret&oacute;rio local
   1. Agora escolha a op&ccedi;atilde;o **Import existing Eclipse Projects**, ![git_wz_existing](/static/git_wizard_existing_project.png)
   1. Deste ponto em diante &eacute; usar o **'Next, Next, Finish'**
   
 ** Bom TDE **
  
   