package edu.cest.conta;

import java.util.Random;

public class ContaCorrente {

	/**
	 * TODO - Corrija deixando os atributos protegidos e acessiveis somente de
	 * acordo com os padroes
	 */
	Random rand = new Random();
	
	private double saldo = 0;
	private double saldoChequeEspecial = rand.nextDouble() * 1000; //NAO ALTERE ESSA CHAMADA
	private String numeroConta;
	
	
	/**
	 * TODO - Implemente o saque para subtrair o valor do saldo.
	 * TODO - Aten&ccedil;&atilde;o o saque s&oacute; pode ser efetuado 
	 * se tiver saldo suficiente na conta <i>MAS</i>, verifique o cheque especial
	 * @return - Saldo apos o saque
	 */
	public double saque(double valorSaque) {
		//TODO - Me implemente corretamente
		if (valorSaque <= saldo) {
			this.saldo -= valorSaque;
		} else if(valorSaque <= saldoChequeEspecial) {
			this.saldoChequeEspecial -= valorSaque;
		}else {
			System.out.println("Saldo Insuficiente!");
		}
		return this.saldo;
	}
	
	/**
	 * TODO - Implemente o dep&oacute;sito
	 * TODO - Aten&ccedil;&atilde;o o dep&oacute;sito n&atilde;o pode ser de valor negativo
	 */
	public double deposito(double valorDeposito) {
		//TODO - Me implemente corretamente
		if (valorDeposito > 0){
			if (this.saldoChequeEspecial < 1000) {
			this.saldoChequeEspecial += valorDeposito;
		}else {
			this.saldo += valorDeposito;
		}
		
		}
		return this.saldo;
	}
		
	public double getSaldo() {
		return saldo;
	}

	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}

	public double getSaldoChequeEspecial() {
		return saldoChequeEspecial;
	}

	public void setSaldoChequeEspecial(double saldoChequeEspecial) {
		this.saldoChequeEspecial = saldoChequeEspecial;
	}

	public String getNumeroConta() {
		return numeroConta;
	}

	public void setNumeroConta(String numeroConta) {
		this.numeroConta = numeroConta;
	}
}