package edu.cest.conta;

public class ContaPoupanca {
	/**
	 *  TODO - Corrija deixando os atributos protegidos e acessiveis somente de
	 *  acordo com os padroes
	 */
	private double saldo = 0;
	private String numeroConta;
	
	public double getSaldo() {
		return saldo;
	}

	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}

	public String getNumeroConta() {
		return numeroConta;
	}

	public void setNumeroConta(String numeroConta) {
		this.numeroConta = numeroConta;
	}

	/**
	 * TODO - Implemente o saque para subtrair o valor do saldo.
	 * TODO - Atenção o saque só pode ser efetuado se tiver saldo suficiente na conta
	 * @return - Saldo apos o saque
	 */
	public double saque(double valorSaque) {
		//TODO - Me implemente corretamente
		if (valorSaque <= saldo) {
			this.saldo = this.saldo - valorSaque;
		}
		return this.saldo;
	}
	
	/**
	 * TODO - Implemente o depósito
	 * TODO - Atenção o depósito não pode ser de falor negativo
	 */
	public double deposito(double valorDeposito) {
		//TODO - Me implemente corretamente
		if (valorDeposito > 0) {
			this.saldo = this.saldo + valorDeposito;
		}
		return this.saldo;
	}
	
}
