package edu.cest.cadastro.endereco;

/**
 * 
 * @author jwalker
 *
 */
public class Cidade {
	private UF estado;
	private String strCidade;
	private int codCidade;

	// TODO - Implemente os getters somente, desse modo o usuário não poderar
	// alterar os dados da cidade
	// Que conceito estamos aplicando? CONCEITO DE ENCAPSULAMENTO
	
	/**
	 * 
	 * @param estado - Objeto UF identificando a que estado a cidade pertence
	 * @param strCidade - Nome da Cidade
	 * @param codCidade - Codigo com 3 letras da Cidade
	 */
	public Cidade(UF estado, String strCidade, int codCidade) {
		super();
		this.estado = estado;
		this.strCidade = strCidade;
		this.codCidade = codCidade;
	}

	public UF getEstado() {
		return estado;
	}

	public void setEstado(UF estado) {
		this.estado = estado;
	}

	public String getStrCidade() {
		return strCidade;
	}

	public void setStrCidade(String strCidade) {
		this.strCidade = strCidade;
	}

	public int getCodCidade() {
		return codCidade;
	}

	public void setCodCidade(int codCidade) {
		this.codCidade = codCidade;
	}

}
