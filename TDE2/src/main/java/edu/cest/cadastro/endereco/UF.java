package edu.cest.cadastro.endereco;

/**
 * 
 * @author jwalker
 *
 */
public class UF {
	private String codUF;
	private String descricao;
	
	/**
	 * TODO - Implemente um construtor que receba o CodUF e a Descricao
	 */
	
	/**
	 * Retorna o codigo da UF
	 * @return
	 */
	
	public UF(String codUF, String descricao) {
		super();
		this.codUF = codUF;
		this.descricao = descricao;
	}
	
	public String getCodUF() {
		return codUF;
	}
	

	/**
	 * Define o codigo da UF - Ex. MA, PI, CE
	 * @param codUF - Sigla do Estado
	 */
	public void setCodUF(String codUF) {
		this.codUF = codUF;
	}
	
	/**
	 * Retorna o nome do Estado
	 * @return
	 */
	public String getDescricao() {
		return descricao;
	}
	
	/**
	 * Define o nome do estado
	 * @param descricao - Nome do Estado
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
}
