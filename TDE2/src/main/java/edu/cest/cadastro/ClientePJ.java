package edu.cest.cadastro;

/**
 * 
 * @author jwalker
 *
 */
public class ClientePJ {

	/**
	 * TODO - Implemente um construtor que receba todos os campos listados (
	 * nomeEmpresa, cnpj ) 
	 * TODO - Implemente os metodos para alteracao e exeibicao dos atributos
	 */
	private String nomeEmpresa;
	private String cnpj;
	
	public ClientePJ(String nomeEmpresa, String cnpj) {
		super();
		this.nomeEmpresa = nomeEmpresa;
		this.cnpj = cnpj;
	}

	public String getNomeEmpresa() {
		return nomeEmpresa;
	}

	public void setNomeEmpresa(String nomeEmpresa) {
		this.nomeEmpresa = nomeEmpresa;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	
	

}
