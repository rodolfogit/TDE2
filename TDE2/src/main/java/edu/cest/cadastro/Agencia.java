package edu.cest.cadastro;

import java.util.ArrayList;

import edu.cest.cadastro.endereco.Cidade;
import edu.cest.conta.ContaCorrente;
import edu.cest.conta.ContaPoupanca;

/**
 * Classe com registros da agencia
 * 
 * @author jwalker
 *
 */
public class Agencia {

	/**
	 * TODO - Implemente os getters e setters de todos os atributos
	 */
	private String codAgencia = null;
	private String strEndereco = null;
	private Cidade cidade;
	
	// TODO - Implemente o incremento de acordo com o tipo de conta
	private int numeroDeCC = 0;
	private int numeroDeCP = 0;

	/**
	 * Lista de Contas Corrente TODO - Implemente de modo a evitar a insercao de
	 * coisas diferentes de CC
	 */
	private ArrayList<ContaCorrente> listaCC = new ArrayList<ContaCorrente>();

	/**
	 * Lista de Contas Poupanca TODO - Implemente de modo a evitar a insercao de
	 * coisas diferentes de CP
	 */
	private ArrayList<ContaPoupanca> listaCP = new ArrayList<ContaPoupanca>();

	/**
	 * TODO - Implemente o construtor para que seja passado o codigo da Agencia
	 * 
	 * @param codAgencia
	 *            - Codigo da Agencia Bancaria
	 */
	public Agencia(String codAgencia) {
		// TODO - Implemente para que seja passado o codigo da Agencia
		this.codAgencia = codAgencia;
	}

	public String getCodAgencia() {
		return codAgencia;
	}

	public void setCodAgencia(String codAgencia) {
		this.codAgencia = codAgencia;
	}

	public String getStrEndereco() {
		return strEndereco;
	}

	public void setStrEndereco(String strEndereco) {
		this.strEndereco = strEndereco;
	}

	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}

	public int getNumeroDeCC() {
		return numeroDeCC;
	}

	public void setNumeroDeCC(int numeroDeCC) {
		this.numeroDeCC = numeroDeCC;
	}

	public int getNumeroDeCP() {
		return numeroDeCP;
	}

	public void setNumeroDeCP(int numeroDeCP) {
		this.numeroDeCP = numeroDeCP;
	}

	public ArrayList<ContaCorrente> getListaCC() {
		return listaCC;
	}

	public void setListaCC(ArrayList<ContaCorrente> contaCorrente) {
		this.listaCC = contaCorrente;
	}

	public ArrayList<ContaPoupanca> getListaCP() {
		return listaCP;
	}

	public void setListaCP(ArrayList<ContaPoupanca> listaCP) {
		this.listaCP = listaCP;
	}
	
	
}
