package edu.cest;

import java.util.ArrayList;
import edu.cest.cadastro.Agencia;
import edu.cest.conta.ContaCorrente;
import edu.cest.conta.ContaPoupanca;

/**
 * Classe para inicializacao do projeto,
 * com o objetivo de rodar a aplicacao via console
 * @author jwalker
 *
 */
public class Principal {
	//TODO - Implemente o inicio do programa
	//TODO - Crie duas ou mais agencias
	//TODO - Insira contas completas
	//TODO - Corrija os pacotes onde estiver errado
	//TODO - Corrija o que for necessário para atender os padrões
	//TODO - Ao fim imprima o total do saldo de todas as contas de cada agencia, 
	//       seguindo os padrões já explicados em sala
	 
	public static void main(String[] args) {
		Agencia ag1 = new Agencia("0337");
		Agencia ag2 = new Agencia("0338");
		
		ContaCorrente cc1 = new ContaCorrente();
		cc1.setNumeroConta("1234-5");
		cc1.setSaldo(500);
		
		ContaPoupanca cp1 = new ContaPoupanca();
		cp1.setNumeroConta("1111-1");
		cp1.setSaldo(300);
		
		
		ContaCorrente cc2 = new ContaCorrente();
		cc2.setNumeroConta("1234-6");
		cc2.setSaldo(200);
		
		ContaPoupanca cp2 = new ContaPoupanca();
		cp2.setNumeroConta("2222-2");
		cp2.setSaldo(800);
		
		
		//TODO - Adicione as contas de acordo
		ArrayList<ContaCorrente> listaCC = new ArrayList<ContaCorrente>();
		listaCC.add(cc1);
		listaCC.add(cc2);
		
		ArrayList<ContaPoupanca> listaCP = new ArrayList<ContaPoupanca>();
		listaCP.add(cp1);
		listaCP.add(cp2);
		
		
		//listaCC.add(cc);
		
		//TODO - a linha abaixo está correta? N�O
		// listaCP.add(cc); ---> listaCP.add(cp);
		
		
		// TODO - Implemente de modo que haja uma lista das agencias
		ArrayList<Agencia> listaAgencias = new ArrayList<Agencia>();
		listaAgencias.add(ag1);
		listaAgencias.add(ag2);
		
		ag1.setListaCC(listaCC);
		ag2.setListaCP(listaCP);
		
		
		// TODO - Implemente de modo que o System.out ao chamar o objeto agencia, 
		//        imprima os dados como Numero da Agencia e quantidade de contas
		
		// TODO - Ao fim imprima o total do saldo de todas as contas de cada agencia, 
		//       seguindo os padrões já explicados em sala
		
		int i=0;
		for (Agencia age: listaAgencias) {
			
			System.out.println("Agencia - Dados");
			System.out.println("Numero Agencia: " + age.getCodAgencia());
			System.out.println("Quantidade de conta: " + listaAgencias.size());
			System.out.println("Saldo CC: " + listaCC.get(i).getSaldo());
			System.out.println("Saldo CP: " + listaCP.get(i).getSaldo());
			System.out.println("-----------------");
			i++;
		}
	}
}
